import java.util.Arrays;

public class CocktailSort {
    public static void main(String[] args) throws InterruptedException {
        int[] v = {32,34,1,8,5,3,9,12,54,12};
        System.out.println(Arrays.toString(v));
        cocktailSort(v);
        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            cocktailSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void cocktailSort(int[] vetor) {
        int inicio, fim, troca, aux;
        inicio = 0;
        fim = vetor.length-1;
        troca = 0;
        while (troca == 0 && inicio < fim) {
            troca = 1;
            for (int i = inicio; i < fim; i = i + 1) {
                if (vetor[i] > vetor[i + 1]) {
                    aux = vetor[i];
                    vetor[i] = vetor[i + 1];
                    vetor[i + 1] = aux;
                    troca = 0;
                }
            }
            fim = fim - 1;
            for (int i = fim; i > inicio; i = i - 1) {
                if (vetor[i] < vetor[i - 1]) {
                    aux = vetor[i];
                    vetor[i] = vetor[i - 1];
                    vetor[i - 1] = aux;
                    troca = 0;
                }
            }
            inicio = inicio + 1;
        }
    }
}
