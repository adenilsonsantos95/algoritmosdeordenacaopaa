import java.util.Arrays;
import java.util.LinkedList;

public class BucketSort {
    public static void main(String[] args) throws InterruptedException {
        int[] v = {32,34,1,8,5,3,9,12,54,12};
        System.out.println(Arrays.toString(v));
        bucketSort(v, v.length);
        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            bucketSort(vetor, vetor.length);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void bucketSort(int[] vetor, int maiorValor){
        int numBaldes = maiorValor/5;

        LinkedList[] B = new LinkedList[numBaldes];

        for (int i = 0; i < numBaldes; i++){ B[i] = new LinkedList<Integer>(); }

        for (int i = 0; i < vetor.length; i++) {
            int j = numBaldes-1;
            while (true){
                if(j<0){ break; }
                if(vetor[i] >= (j*5)){
                    B[j].add(vetor[i]);
                    break;
                }
                j--;
            }
        }
        int indice = 0;
        for (int i = 0; i < numBaldes; i++){
            int[] aux = new int[B[i].size()];
            for (int j = 0; j < aux.length; j++){ aux[j] = (Integer)B[i].get(j); }
            InsertionSort.insertionSort(aux);
            for (int j = 0; j < aux.length; j++, indice++){ vetor[indice] = aux[j]; }
        }
    }
}
