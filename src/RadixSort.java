import java.util.Arrays;

public class RadixSort {

    public static void main(String[] args) throws InterruptedException {
        int[] v = {32,34,1,8,5,3,9,12,54,12};
        System.out.println(Arrays.toString(v));
        radixSort(v);
        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            radixSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void radixSort(int[] vetor){
        for(int digit = 0; digit < 3; digit ++){
            int power = (int) Math.pow(10, digit+1);

            int z[][] = new int[vetor.length][10];
            int n[] = new int[10];

            for(int i = 0; i < vetor.length; i ++){
                int num = vetor[i];
                z[n[(num%power)/(power/10)]][(num%power)/(power/10)] = num;
                n[(num%power)/(power/10)]++;

            }
            int c = 0;
            for(int i = 0; i < 10; i ++){

                for(int j = 0; j < vetor.length; j ++){
                    if(j < n[i]){
                        vetor[c] = z[j][i];
                        c++;
                    }else{break;}
                }
            }

        }
    }
}
