import java.util.Arrays;

public class CountingSort {
    public static void main(String[] args) throws InterruptedException {
        int[] v = {32,34,1,8,5,3,9,12,54,12};
        System.out.println(Arrays.toString(v));
        v = countingSort(v);
        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            countingSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static int[] countingSort(int[] vetor) {
        int maior = vetor[0];
        for (int i = 1; i < vetor.length; i++) {
            if (vetor[i] > maior) { maior = vetor[i]; }
        }
        int[] c = new int[maior+1];
        for (int i = 0; i < vetor.length; i++) { c[vetor[i]] += 1; }

        for (int i = 1; i < c.length; i++) { c[i] += c[i-1]; }

        int[] b = new int[vetor.length];
        for (int i = b.length-1; i >= 0; i--) {
            b[c[vetor[i]] -1] = vetor[i];
            c[vetor[i]]--;
        }
        return b;
    }
}
