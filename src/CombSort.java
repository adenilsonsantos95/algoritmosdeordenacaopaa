import java.util.Arrays;

public class CombSort {
    public static void main(String[] args) throws InterruptedException {
//        int[] v = {32,34,1,8,5,3,9,12,54,12};
//        System.out.println(Arrays.toString(v));
//        combSort(v);
//        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            combSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void combSort(int[] vetor){
        int tamanho = vetor.length;
        boolean trocado = true;
        while (tamanho > 1 || trocado){
            if (tamanho > 1){
                tamanho = (int) (tamanho / 1.247330950103979);
            }

            int i = 0;
            trocado = false;
            while (i + tamanho < vetor.length){
                if (vetor[i] > vetor[i + tamanho]){
                    int aux = vetor[i];
                    vetor[i] = vetor[i + tamanho];
                    vetor[i + tamanho] = aux;
                    trocado = true;
                }
                i++;
            }
        }
    }
}
