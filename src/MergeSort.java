public class MergeSort {

    public static void main(String[] args) throws InterruptedException {
//        int[] v = {32,34,1,8,5,3,9,12,54,12};
//        System.out.println(Arrays.toString(v));
//        ordena(v,0, v.length-1);
//        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            mergeSort(vetor, 0, vetor.length-1);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }

    }

    public static void mergeSort(int[] vetor, int indiceInicio, int indiceFim) {
        if (vetor != null && indiceInicio < indiceFim && indiceInicio >= 0 &&
                indiceFim < vetor.length && vetor.length != 0) {
            int meio = ((indiceFim + indiceInicio) / 2);
            mergeSort(vetor, indiceInicio, meio);
            mergeSort(vetor, meio + 1, indiceFim);

            merge(vetor, indiceInicio, meio, indiceFim);
        }

    }

    public static void merge(int[] vetor, int indiceInicio, int meio, int indiceFim) {
        int[] auxiliar = new int[vetor.length];
        for (int i = indiceInicio; i <= indiceFim; i++) {
            auxiliar[i] = vetor[i];
        }

        int i = indiceInicio;
        int j = meio + 1;
        int k = indiceInicio;

        while (i <= meio && j <= indiceFim) {
            if (auxiliar[i] < auxiliar[j]) {
                vetor[k] = auxiliar[i];
                i++;
            } else {
                vetor[k] = auxiliar[j];
                j++;
            }
            k++;
        }
        while (i <= meio) { vetor[k] = auxiliar[i]; i++; k++; }
        while (j <= indiceFim) { vetor[k] = auxiliar[j]; j++; k++; }
    }
}
