import java.io.IOException;

public class InsertionSort {
    public static void main(String[] args) throws IOException, InterruptedException {

        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            insertionSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }


    public static void insertionSort(int[] vetor){

        for (int i = 1; i < vetor.length; i++){

            int aux = vetor[i];
            int j = i;

            while ((j > 0) && (vetor[j-1] > aux)){
                vetor[j] = vetor[j-1];
                j -= 1;
            }
            vetor[j] = aux;

        }

    }
}
