import java.util.Arrays;

public class GnomeSort {
    public static void main(String[] args) throws InterruptedException {
        int[] v = {32,34,1,8,5,3,9,12,54,12};
        System.out.println(Arrays.toString(v));
        gnomeSort(v);
        System.out.println(Arrays.toString(v));
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            gnomeSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void gnomeSort(int[] vetor){
        int pivo = 0;
        int aux;
        while(pivo<(vetor.length-1)){
            if(vetor[pivo]>vetor[pivo+1]){
                aux = vetor[pivo];
                vetor[pivo] = vetor[pivo+1];
                vetor[pivo+1] = aux;
                if(pivo>0){
                    pivo-=2;
                }
            }
            pivo++;
        }
    }
}
