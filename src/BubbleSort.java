import javax.sql.rowset.CachedRowSet;
import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) throws InterruptedException {
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            bubbleSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void bubbleSort(int[] vetor){
        boolean trocado;
        do {
           trocado = false;
            for (int i = 0; i < vetor.length - 1; i++) {
                if (vetor[i] > vetor[i+1]){
                    int aux = vetor[i];
                    vetor[i] = vetor[i+1];
                    vetor[i+1] = aux;
                    trocado = true;
                }
            }

        }while (trocado);
    }
}
