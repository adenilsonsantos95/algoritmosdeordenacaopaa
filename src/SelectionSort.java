public class SelectionSort {
    public static void main(String[] args) throws InterruptedException {
        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500000;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            selectionSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void selectionSort(int[] vetor){
        for (int i = 0; i < vetor.length; ++i) {
            int menorIndice = i;
            for (int j = i+1; j < vetor.length; ++j) {
                if (vetor[j] < vetor[menorIndice]){
                    menorIndice = j;
                }
            }
            int aux = vetor[i];
            vetor[i] = vetor[menorIndice];
            vetor[menorIndice] = aux;
        }
    }
}
