import java.util.Arrays;

public class BogoSort {
    public static void main(String[] args) throws InterruptedException {
        int[] v = {32,34,1,8,5,3,9,12,54,12};
        System.out.println(Arrays.toString(v));
        bogoSort(v);
        System.out.println(Arrays.toString(v));





        for (int j = 0; j < 15; j++) {
            Thread.sleep(100);
            int quantidade = 500;
            int[] vetor = new int[quantidade];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = (int) (Math.random()*quantidade);
            }

            long tempoInicial = System.currentTimeMillis();

            bogoSort(vetor);

            long tempoFinal = System.currentTimeMillis();

            System.out.println((tempoFinal - tempoInicial));
        }
    }

    public static void bogoSort(int[] vetor) {
        int embaralhamento = 1;
        for(; !estaSorteado(vetor); embaralhamento++)
            embaralhamento(vetor);
    }
    public static void embaralhamento(int[] vetor) {
        int i=vetor.length-1;
        while(i>0)
            troca(vetor,i--,(int)(Math.random()*i));
    }
    public static void troca(int[] vetor, int i, int j) {
        int aux = vetor[i];
        vetor[i]=vetor[j];
        vetor[j] = aux;
    }
    public static boolean estaSorteado(int[] vetor) {

        for(int i=1;i<vetor.length;i++)
            if(vetor[i]<vetor[i-1])
                return false;
        return true;
    }
}
